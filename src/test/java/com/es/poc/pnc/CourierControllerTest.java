package com.es.poc.pnc;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.es.poc.pnc.service.UserService;

@RunWith(SpringRunner.class)
@EnableAutoConfiguration 
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes=CourierControllerTest.class)
public class CourierControllerTest{
	@MockBean
    private UserService userService;

    @Test
    public void contexLoads() throws Exception {
        assertThat(userService).isNotNull();
    }
}
