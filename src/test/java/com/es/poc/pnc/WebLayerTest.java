package com.es.poc.pnc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.es.poc.pnc.service.EmailService;
import com.es.poc.pnc.service.SecurityService;
import com.es.poc.pnc.service.UserService;
import com.es.poc.pnc.validator.UserValidator;

@RunWith(SpringRunner.class)
@EnableAutoConfiguration 
@WebMvcTest
public class WebLayerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserService service;
    @MockBean
    private EmailService emailService;
    @MockBean
    private SimpleMailMessage simpleMailMessage;
    @MockBean
    private SecurityService securityService;
    @MockBean
    private UserValidator UserValidator;
    @Test
    public void shouldReturnDefaultMessage() throws Exception {
        this.mockMvc.perform(get("/login")).andDo(print()).andExpect(status().is(401));
    }
}