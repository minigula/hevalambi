<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<body>
	<jsp:include page="/navbar.jsp"></jsp:include>
	<div id="wrapper">
		<div id="page-content-wrapper">
			<div id="demo" class="container-fluid collapse in">
				<form:form method="POST" modelAttribute="courier">
					<div class="container-fluid mg-btm-20">
						<div class="row">
							<div class="form-section-h1 text-capitalize bb-blue">
								<p class="margin-10">
									<strong>Bookings List: 
								</p>
								* Click on ID to see item list
								</strong
							</div>
						</div>
						<div class="panel panel-default panel-table">
							<div class="panel-body">
								<div class="table-responsive">
									<table id="couriersTable"
										class="table table-striped table-bordered table-list">
										<thead>
											<tr>
												<th>Sr. No.</th>
												<th>Id</th>
												<th>From Address</th>
												<th>To Address</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${courier.couriers}" var="courier"
												varStatus="courierList">
												<tr>
													<td><c:out value="${courierList.index+1}" /></td>
													<td><a href="${courier.id}"><c:out value="${courier.id}" /></a></td>
													<td><c:out value="${courier.fromAddress}" /></td>
													<td><c:out value="${courier.toAddress}" /></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	$(document).ready(
			function() {
				var table = $('#couriersTable').DataTable({
					lengthChange : false,
					"order" : [ [ 2, "desc" ] ],
					buttons : [ {
						extend : 'colvis',
						postfixButtons : [ 'colvisRestore' ]
					} ],
					"bInfo" : false,
					postfixButtons : [ 'colvisRestore' ],
					//collectionLayout: 'fixed two-column',
					//columns: ':not(:first-child)',
					language : {
						buttons : {
							colvis : 'Select Column'
						}
					}
				});

				table.buttons().container().appendTo(
						'#couriersTable_wrapper .col-sm-6:eq(0)');
			});
</script>
</html>