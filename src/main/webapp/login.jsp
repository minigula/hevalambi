<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="robots" content="noindex">
<title>Pick N Courier</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<jsp:include page="/navbar.jsp"></jsp:include><div id="wrapper">
	<form:form method="POST" modelAttribute="userForm" class="form-signin">
		<div id="demo" class="container-fluid collapse in">
			<div class="container-fluid land-infobar mgtop-10">
				<div class="row">
					<div class="col-md-12">
						<h3>
							<strong>Create your account:</strong>
						</h3>
					</div>
				</div>
			</div>
			<div class="panel-margin">
				<div class="container-fluid mg-btm-20">
					<div class="row">
						<div class="form-section-h1 text-capitalize bb-blue">
							<br />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<form method="POST" action="${contextPath}/login"
									class="form-signin">
									<h2 class="form-heading">Log in</h2>
									<div class="form-group ${error != null ? 'has-error' : ''}">
										<span>${message}</span> <input name="username" type="text"
											class="form-control" placeholder="Username" autofocus="true" />
										<input name="password" type="password" class="form-control"
											placeholder="Password" /> <span>${error}</span> <input
											type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}" />

										<button class="btn btn-lg btn-primary btn-block" type="submit">Log
											In</button>
										<h4 class="text-center">
											<a href="${contextPath}/registration">Create an account</a>
										</h4>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
</form:form>
</body>
</html>
