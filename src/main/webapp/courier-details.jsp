<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<body>
	<jsp:include page="/navbar.jsp"></jsp:include>
	<div id="wrapper">
		<div id="page-content-wrapper">
			<div id="demo" class="container-fluid collapse in">
				<div class="container-fluid land-infobar mgtop-10 mgbottom-10">
					<div class="row">
						<p class="margin-10">
							<strong>Booking Details:</strong> </span>
						</p>
					</div>
				</div>
				<form:form method="POST" modelAttribute="courier">
					<div class="panel-margin">
						<!-- Training & Competency -->
						<div class="container-fluid mg-btm-10">
							<div class="row"></div>
							<div class="row">
								<div class="col-sm-3">
									<div class="form-group">
										<label class="control-label" for="INC_LOC_GROUP">To
											Address</label></br>
										<c:out value="${courier.toAddress}" />
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<label class="control-label" for="INC_LOCATION">From
											Address</label></br>
										<c:out value="${courier.fromAddress}" />
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="container-fluid mg-btm-20">
						<div class="row">
							<div class="form-section-h1 text-capitalize bb-blue">
								<p class="margin-10">
									<strong>Items List: 
								</p>
								</strong
							</div>
						</div>
						<div class="panel panel-default panel-table">
							<div class="panel-body">
								<div class="table-responsive">
									<table id="itemsTable"
										class="table table-striped table-bordered table-list">
										<thead>
											<tr>
												<th>Sr. No.</th>
												<th>Name</th>
												<th>Weight</th>
												<th>Dimension</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${courier.items}" var="item"
												varStatus="itemsList">
												<tr>
													<td><c:out value="${itemsList.index+1}" /></td>
													<td><c:out value="${item.name}" />
													<td><c:out value="${item.weight}" /></td>
													<td><c:out value="${item.dim}" /></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	$(document).ready(
			function() {
				var table = $('#itemsTable').DataTable({
					lengthChange : false,
					"order" : [ [ 2, "desc" ] ],
					buttons : [ {
						extend : 'colvis',
						postfixButtons : [ 'colvisRestore' ]
					} ],
					"bInfo" : false,
					postfixButtons : [ 'colvisRestore' ],
					//collectionLayout: 'fixed two-column',
					//columns: ':not(:first-child)',
					language : {
						buttons : {
							colvis : 'Select Column'
						}
					}
				});

				table.buttons().container().appendTo(
						'#tasksTable_wrapper .col-sm-6:eq(0)');
			});
</script>
</ html>