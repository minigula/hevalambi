<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="en">
<head>
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<link href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css"
	rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<style type="text/css">
/* Fixed navbar */
body {
	padding-top: 90px;
}
/* General sizing */
ul.dropdown-lr {
	width: 300px;
}

/* mobile fix */
@media ( max-width : 768px) {
	.dropdown-lr h3 {
		color: #eee;
	}
	.dropdown-lr label {
		color: #eee;
	}
}
</style>
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
	window.alert = function() {
	};
	var defaultCSS = document.getElementById('bootstrap-css');
	function changeCSS(css) {
		if (css)
			$('head > link')
					.filter(':first')
					.replaceWith(
							'<link rel="stylesheet" href="'+ css +'" type="text/css" />');
		else
			$('head > link').filter(':first').replaceWith(defaultCSS);
	}
	$(document).ready(function() {
		var iframe_height = parseInt($('html').height());
	});
</script>
</head>
<body>
<link href="http://cdn.phpoll.com/css/animate.css" rel="stylesheet">
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">Pick N Courier</a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="/">Home</a></li>
				<sec:authorize access="isAuthenticated()">
					<li><a href="/courier/all">Couriers List</a></li>
					</sec:authorize>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<sec:authorize access="isAuthenticated()">
					<li>
						<form action="${pageContext.request.contextPath}/logout"
							method="POST">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
							<button type="submit" class="btn btn-default navbar-btn">
								Logout</button>
						</form>
					</li>
				</sec:authorize>
				<sec:authorize access="isAnonymous()">
					<li><a href="${contextPath}/registration">Register <span></span>
					</a>
					<li><a href="${contextPath}/login">Log In <span></span></a>
					</li>
				</sec:authorize>
			</ul>
			<form id="searchCourier" class="navbar-form navbar-right"
				action="${contextPath}/courier/search" role="search">
				<div class="input-group">
					<input id="searchValue" name="id" type="text" class="form-control"
						placeholder="Search booking with Id..." required ="true"><span
						class="input-group-btn">
						<button type="reset" class="btn btn-default">
							<span class="glyphicon glyphicon-remove"> <span
								class="sr-only">Close</span>
							</span>
						</button>
						<button id="searchCourierBtn" type="submit"
							class="btn btn-default">
							<span class="glyphicon glyphicon-search"> <span
								class="sr-only">Search</span>
							</span> <input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
						</button>
					</span>
				</div>
			</form>
		</div>
	</div>
</nav>
</body>
</html>
