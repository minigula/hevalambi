<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
	<jsp:include page="/navbar.jsp"></jsp:include><div id="wrapper">
		<form:form method="POST" modelAttribute="userForm" class="form-signin">
			<div id="demo" class="container-fluid collapse in">
				<div class="container-fluid land-infobar mgtop-10">
					<div class="row">
						<div class="col-md-12">
							<h3>
								<strong>Create your account:</strong>
							</h3>
						</div>
					</div>
				</div>
				<div class="panel-margin">
					<div class="container-fluid mg-btm-20">
						<div class="row">
							<div class="form-section-h1 text-capitalize bb-blue">
								<br />
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									<spring:bind path="username">
										<div class="form-group ${status.error ? 'has-error' : ''}">
											<form:input type="text" path="username" class="form-control"
												placeholder="Username" autofocus="true"></form:input>
											<form:errors path="username"></form:errors>
										</div>
									</spring:bind>
									<spring:bind path="email">
										<div class="form-group ${status.error ? 'has-error' : ''}">
											<form:input type="email" path="email" class="form-control"
												placeholder="Email"></form:input>
											<form:errors path="email"></form:errors>
										</div>
									</spring:bind>
									<spring:bind path="password">
										<div class="form-group ${status.error ? 'has-error' : ''}">
											<form:input type="password" path="password"
												class="form-control" placeholder="Password"></form:input>
											<form:errors path="password"></form:errors>
										</div>
									</spring:bind>

									<spring:bind path="passwordConfirm">
										<div class="form-group ${status.error ? 'has-error' : ''}">
											<form:input type="password" path="passwordConfirm"
												class="form-control" placeholder="Confirm your password"></form:input>
											<form:errors path="passwordConfirm"></form:errors>
										</div>
									</spring:bind>
									<div class="container-fluid">
										<div class="row">
											<div class="col-md-12 text-right mgbottom-20">
												<button class="btn btn-lg btn-primary btn-block"
													type="submit">Submit</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form:form>
</body>
</html>
