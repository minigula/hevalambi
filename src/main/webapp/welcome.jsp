<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="en">
<head></head>
<body>
	<jsp:include page="/navbar.jsp"></jsp:include>
	<form:form method="POST" action="courier/add"
		modelAttribute="courierForm">
		<div id="wrapper">
			<div id="demo" class="container-fluid collapse in">
				<div class="container-fluid land-infobar mgtop-10">
					<div class="row">
						<div class="col-md-12">
							<h3>
								<strong>Booking section:</strong> 
							</h3>
						</div>
					</div>
				</div>
				<div class="panel-margin">
					<div class="container-fluid mg-btm-20">
						<div class="row">
							<div class="form-section-h1 text-capitalize bb-blue">
							<br/>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									<label class="control-label">From Address<span
										class="required">*</span></label>
									<form:input path="fromAddress"
										requiredclass="form-control input-sm" required ="true"/>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label class="control-label">To Address<span
										class="required">*</span></label>
									<form:input path="toAddress"
										requiredclass="form-control input-sm" required ="true"/>
								</div>
							</div>
						</div>
					</div>
					<div class="container-fluid mg-btm-10">
						<div class="row">
							<div class="form-section-h2 text-capitalize bb-blue"><h3>Add
								Items<h3></div>
						</div>
						<div class="table-border table-responsive">
							<table id="itemTable"
								class="table table-condensed table-striped table-bordered no-margin">
								<thead>
									<tr>
										<th>Item Name<span class="required">*</span></th>
										<th>Weight<span class="required">*</span></th>
										<th class="mwidth-10">Dimension<span class="required">*</span></th>
										<th class="tbl-item-col">&nbsp;</th>
									</tr>
								</thead>
								<tbody>
									<c:choose>
										<c:when test="${courierForm.id != null}">
											<c:forEach items="${courierForm.items}" var="act"
												varStatus="itemList">
												<tr>
													<td>
													</td>
													<td><div class="form-group no_margin">
															<form:input path="items[${itemList.index}].name"
																value="${items[itemList.index].name}" name="ITEM_NAME"
																type="text" />
														</div></td>
													<td>
														<div class="form-group no_margin">
															<form:input path="items[${itemList.index}].weight"
																id="items${itemList.index}_weight"
																cssClass="form-control"
																value="${items[itemList.index].weight}" />
														</div>
													</td>
													<td><div class="form-group no_margin">
															<form:input path="items[${itemList.index}].dim"
																name="DIM"
																value="${items[itemList.index].dim}"
																cssClass="form-control selectpicker"
																id="status${itemList.index}" title=""
																data-live-search="false"/>
														</div></td>
													<td><button type="button"
															class="btn btn-link no_padding"
															onclick="deleteRowDynamically('itemTable')">
															<img src="${contextPath}/resources/img/delete.svg" alt="" />
														</button>
														<button type="button" class="btn btn-link no_padding"
															onclick="addRowDynamically('itemTable')">
															<img src="${contextPath}/img/plus.svg" alt="" />
														</button></td>
												</tr>
											</c:forEach>
										</c:when>
										<c:when test="${courierForm.id == null}">
											<tr>
												<td><div class="form-group no_margin">
														<form:input path="items[0].name" value="${items[0].name}"
															required="true" name="ITEM_NAME" type="text"
															cssClass="form-control input-sm" id="itemName0" />
													</div>
													<div>
														<form:errors path="items[0].name" class="required" />
													</div></td>
												<td><div class="form-group no_margin">

														<form:input path="items[0].weight"
															id="items0_weight" required="true"
															cssClass="form-control" />
													</div></td>
												<td><div class="form-group no_margin">
														<form:input path="items[0].dim"
															id="items0_weight" required="true"
															cssClass="form-control" />
														<div>
															<form:errors path="items[0].dim" class="required" />
														</div>
													</div></td>
												<td><button type="button"
														class="btn btn-link no_padding"
														onclick="deleteRowDynamically('itemTable')">
														<img src="${contextPath}/img/delete.svg" alt="" />
													</button>
													<button type="button" class="btn btn-link no_padding"
														onclick="addRowDynamically('itemTable')">
														<img src="${contextPath}/img/plus.svg" alt="" />
													</button></td>
											</tr>
										</c:when>
									</c:choose>
								</tbody>
							</table>
						</div>
					</div>
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12 text-right mgbottom-20">
								<input class="btn btn-primary" type="submit" value="Submit" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer">
			<div class="small">Copyright � 2017 Company All rights reserved</div>
		</div>
	</form:form>
</body>
<script type="text/javascript" charset="utf-8">
	function deleteRowDynamically(tableID) {
		var current = window.event.srcElement;
		var table = document.getElementById(tableID).tBodies[0];
		var rowCount = table.rows.length;
		for (var i = 1; i < rowCount; i++) {
			while ((current = current.parentElement) && current.tagName != "TR")
				;
			current.parentElement.removeChild(current);
			count(tableID);
		}
	}
	
	function addRowDynamically(tableID) {
		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var colCount = table.rows[0].cells.length;
		var txt = "";
		txt = rowCount;
		var increment = 2;
		for (var i = 0; i < colCount; i++) {
			var newcell = row.insertCell(i);
			newcell.innerHTML = table.rows[1].cells[i].innerHTML;
			var html = newcell.innerHTML;
			var rc = parseInt(rowCount);
			rc = rc - 1;
			html = html.replace(/\[0\]/g, "[" + rc + "]");
			newcell.innerHTML = "";
			newcell.innerHTML = html;
			for (var j = 0; j < newcell.childNodes.length; j++) {
				if (newcell.childNodes[j] != 'undefined') {
					switch (newcell.childNodes[j].type) {
					case "text":
						newcell.childNodes[j].value = "";
						newcell.childNodes[j].id = "input" + rowCount;
						newcell.childNodes[j].value = txt;
						txt = "";
						break;
					case "checkbox":
						newcell.childNodes[j].checked = false;
						newcell.childNodes[j].id = newcell.childNodes[j].id
								+ rowCount;
						break;
					case "select-one":
						newcell.childNodes[j].selectedIndex = 0;
						newcell.childNodes[j].options = "";
						newcell.childNodes[j].id = newcell.childNodes[j].id
								+ rowCount;
						break;
					}
				}
				var divElement = newcell.childNodes[j];
				if (divElement.childNodes.length == 0) {
					if (divElement.type == 'hidden') {
						divElement.value = "";
					}
				}
				for (k = 0; k < divElement.childNodes.length; k++) {
					switch (divElement.childNodes[k].type) {

					case "text":
						divElement.childNodes[k].value = "";
						divElement.childNodes[k].id = divElement.childNodes[k].id
								+ "_" + rowCount;
						divElement.childNodes[k].value = "";
						txt = "";
						break;
					case "checkbox":

						divElement.childNodes[k].checked = false;
						divElement.childNodes[k].id = divElement.childNodes[k].id
								+ "_" + rowCount;
						break;
					case "hidden":
						divElement.childNodes[k].value = "";
						divElement.childNodes[k].id = divElement.childNodes[k].id
								+ "_" + rowCount;
						break;
					case "number":
						divElement.childNodes[k].value = "";
						divElement.childNodes[k].id = divElement.childNodes[k].id
								+ "_" + rowCount;
						break;
					}
				}
			}
		}
		count(tableID);
	}
</script>
</html>