package com.es.poc.pnc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * @author srinivas.minigula
 *
 */
@EnableAutoConfiguration 
@SpringBootApplication
public class PNCWebApplication extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(PNCWebApplication.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(PNCWebApplication.class, args);
	}
}
