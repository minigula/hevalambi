package com.es.poc.pnc.service;

import com.es.poc.pnc.model.User;

/**
 * @author srinivas.minigula
 *
 */
public interface UserService {
	/**
	 * @param user
	 */
	void save(User user);

	/**
	 * Find user by name
	 * @param username
	 * @return User
	 */
	User findByUsername(String username);

	/**
	 * Find user by email
	 * @param email
	 * @return User
	 */
	User findByEmail(String email);

	/**
	 * Find user by name or email
	 * @param username
	 * @param email
	 * @return User
	 */
	User findUserByUsernameOrEmail(String username, String email);

}
