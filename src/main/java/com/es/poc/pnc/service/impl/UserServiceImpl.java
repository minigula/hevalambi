package com.es.poc.pnc.service.impl;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.es.poc.pnc.model.Courier;
import com.es.poc.pnc.model.User;
import com.es.poc.pnc.repository.RoleRepository;
import com.es.poc.pnc.repository.UserRepository;
import com.es.poc.pnc.service.UserService;

/**
 * @author srinivas.minigula
 *
 */
@Service("userService")
public class UserServiceImpl implements UserService {
   
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
    	this.userRepository=userRepository;
	}

	/**
     * @see com.es.poc.pnc.service.UserService#save(com.es.poc.pnc.model.User)
     */
    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(new HashSet<>(roleRepository.findAll()));
        user.setCourier(new HashSet<Courier>());
        userRepository.save(user);
    }

    /**
     * @see com.es.poc.pnc.service.UserService#findByUsername(java.lang.String)
     */
    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
    /**
     * @see com.es.poc.pnc.service.UserService#findByEmail(java.lang.String)
     */
    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }
    
    /**
     * @see com.es.poc.pnc.service.UserService#findUserByUsernameOrEmail(java.lang.String, java.lang.String)
     */
    @Override
    public User findUserByUsernameOrEmail(String username, String email) {
        return userRepository.findByUsernameOrEmail(username, email);
    }
}
