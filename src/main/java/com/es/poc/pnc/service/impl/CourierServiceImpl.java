package com.es.poc.pnc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.es.poc.pnc.model.Courier;
import com.es.poc.pnc.model.User;
import com.es.poc.pnc.repository.CourierRepository;
import com.es.poc.pnc.service.CourierService;

/**
 * @author srinivas.minigula
 *
 */
@Service("courierService")
public class CourierServiceImpl implements CourierService {
	@Autowired
	private CourierRepository courierRepository;

	/**
	 * @see com.es.poc.pnc.service.CourierService#findByUser(com.es.poc.pnc.model.User)
	 */
	@Override
	public List<Courier> findByUser(User user) {
		return courierRepository.findByUser(user);
	}

	/**
	 * @see com.es.poc.pnc.service.CourierService#findById(java.lang.Long)
	 */
	@Override
	public Courier findById(Long id) {
		return courierRepository.findById(id);
	}

	/**
	 * @see com.es.poc.pnc.service.CourierService#save(com.es.poc.pnc.model.Courier)
	 */
	@Override
	public Courier save(Courier courier) {
		return courierRepository.save(courier);
	}
}
