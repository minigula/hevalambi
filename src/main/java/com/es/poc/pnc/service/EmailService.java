package com.es.poc.pnc.service;

/**
 * @author srinivas.minigula
 *
 */
public interface EmailService {
	/**
	 * Simple Email Text Message 
	 * @param to
	 * @param subject
	 * @param text
	 */
	void sendSimpleMessage(String to, String subject, String text);

}