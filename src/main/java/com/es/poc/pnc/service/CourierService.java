package com.es.poc.pnc.service;

import java.util.List;

import com.es.poc.pnc.model.Courier;
import com.es.poc.pnc.model.User;

/**
 * Courier Services
 * @author srinivas.minigula
 *
 */
public interface CourierService {
	List<Courier> findByUser(User user);
	Courier findById(Long id);
	Courier save(Courier courier);
}
