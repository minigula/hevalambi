package com.es.poc.pnc.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.es.poc.pnc.service.EmailService;

/**
 * @author srinivas.minigula
 *
 */
@Service("emailService")
public class EmailServiceImpl implements EmailService {

	@Autowired
	public JavaMailSender emailSender;

	/**
	 * @see com.es.poc.pnc.service.EmailService#sendSimpleMessage(java.lang.String,
	 *      java.lang.String, java.lang.String)
	 */
	public void sendSimpleMessage(String to, String subject, String text) {
		try {
			SimpleMailMessage message = new SimpleMailMessage();
			message.setTo(to);
			message.setSubject(subject);
			message.setText(text);
			emailSender.send(message);
		} catch (MailException exception) {
			exception.printStackTrace();
		}
	}

}