package com.es.poc.pnc.service;

/**
 * @author srinivas.minigula
 *
 */
public interface SecurityService {
    /**
     * Finds Logged in user name
     * @return String username
     */
    String findLoggedInUsername();

    /**
     * Auto login the user
     * @param username
     * @param password
     */
    void autologin(String username, String password);
}
