package com.es.poc.pnc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.es.poc.pnc.model.User;

/**
 * user repository
 * 
 * @author srinivas.minigula
 *
 */
public interface UserRepository extends JpaRepository<User, Long> {
	/**
	 * Finds user By name
	 * 
	 * @param username
	 * @return
	 */
	User findByUsername(String username);

	/**
	 * Find user by email
	 * 
	 * @param username
	 * @return
	 */
	User findByEmail(String username);

	/**
	 * find user by Name and Email
	 * 
	 * @param username
	 * @param emailId
	 * @return
	 */
	public User findByUsernameOrEmail(String username, String emailId);
}
