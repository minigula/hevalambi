package com.es.poc.pnc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.es.poc.pnc.model.Courier;
import com.es.poc.pnc.model.Item;

/**
 * Item Repository
 * 
 * @author srinivas.minigula
 *
 */
public interface ItemRepository extends JpaRepository<Item, Long> {
	/**
	 * Fetches Item by Courier
	 * 
	 * @param courier
	 * @return List of Couriers
	 */
	List<Item> findByCourier(Courier courier);
}
