package com.es.poc.pnc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.es.poc.pnc.model.Role;

/**
 * Role Repository
 * @author srinivas.minigula
 *
 */
public interface RoleRepository extends JpaRepository<Role, Long>{
}
