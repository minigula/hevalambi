package com.es.poc.pnc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.es.poc.pnc.model.Courier;
import com.es.poc.pnc.model.User;

/**
 * Courier Repository
 * @author srinivas.minigula
 *
 */
public interface CourierRepository extends JpaRepository<Courier, Long> {
	List<Courier> findByUser(User user);
	Courier findById(Long id);
}
