package com.es.poc.pnc.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.es.poc.pnc.model.User;
import com.es.poc.pnc.service.UserService;

/**
 * Validations for User Registration and Login
 * @author srinivas.minigula
 *
 */
@Component
public class UserValidator implements Validator {
	@Autowired
	private UserService userService;

	@Override
	public boolean supports(Class<?> aClass) {
		return User.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		User user = (User) o;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty");
		if (user.getUsername().length() < 6 || user.getUsername().length() > 32) {
			errors.rejectValue("username", "Size.userForm.username");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty");
		if (user.getEmail().length() < 4) {
			errors.rejectValue("email", "Size.userForm.email");
		}
		if (emailExist(user.getEmail())) {
			errors.rejectValue("email", "There is an account with this email adress");
		}
		if (userService.findByUsername(user.getUsername()) != null) {
			errors.rejectValue("username", "Duplicate.userForm.username");
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
		if (user.getPassword().length() < 8 || user.getPassword().length() > 32) {
			errors.rejectValue("password", "Size.userForm.password");
		}

		if (!user.getPasswordConfirm().equals(user.getPassword())) {
			errors.rejectValue("passwordConfirm", "Diff.userForm.passwordConfirm");
		}
		User existingUser = userService.findUserByUsernameOrEmail(user.getUsername(), user.getEmail());
		if (existingUser != null) {
			if (existingUser.getUsername().equalsIgnoreCase(user.getUsername()))
				errors.rejectValue("email", "Username already exists");

			if (existingUser.getEmail().equalsIgnoreCase(user.getEmail()))
				errors.rejectValue("email", "Email id already exists");
		}
	}

	/**
	 * @param email
	 * @return
	 */
	private boolean emailExist(String email) {
		User user = userService.findByEmail(email);
		if (user != null) {
			return true;
		}
		return false;
	}
}
