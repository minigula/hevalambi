package com.es.poc.pnc.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity Represents Item properties
 * 
 * @author srinivas.minigula
 *
 */
@Entity
@Table(name = "item")
public class Item {
	private Long id;
	private String name;
	private String weight;
	private String dim;
	private Courier courier;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "courier", nullable = false)
	public Courier getCourier() {
		return courier;
	}

	public void setCourier(Courier courier) {
		this.courier = courier;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getDim() {
		return dim;
	}

	public void setDim(String dim) {
		this.dim = dim;
	}

	@Override
	public String toString() {
		return "Item [id=" + id + ", name=" + name + ", weight=" + weight + ", dim=" + dim + ", courier=" + getCourier()
				+ "]";
	}

}
