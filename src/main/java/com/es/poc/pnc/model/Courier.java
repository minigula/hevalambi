package com.es.poc.pnc.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author srinivas.minigula
 *
 */
@Entity
@Table(name = "courier")
@JsonIgnoreProperties({ "user", "items" })
public class Courier {
	private Long id;

	private String toAddress;
	private String fromAddress;
	private List<Item> items = new ArrayList<>();
	private User user;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user", nullable = false)
	public User getUser() {
		return user;
	}

	@OneToMany(cascade = CascadeType.ALL, targetEntity = Item.class, mappedBy = "courier", fetch = FetchType.EAGER)
	public List<Item> getItems() {
		return items;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "Courier [id=" + id + ", toAddress=" + toAddress + ", fromAddress=" + fromAddress + ", items=" + "]";
	}

}
