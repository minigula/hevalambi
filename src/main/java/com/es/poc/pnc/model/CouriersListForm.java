package com.es.poc.pnc.model;

import java.util.List;

/**
 * Courier List form to display courier information
 * 
 * @author srinivas.minigula
 *
 */
public class CouriersListForm {
	public List<Courier> couriers;

	public List<Courier> getCouriers() {
		return couriers;
	}

	public void setCouriers(List<Courier> couriers) {
		this.couriers = couriers;
	}
}
