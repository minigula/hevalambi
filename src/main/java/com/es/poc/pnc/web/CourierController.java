package com.es.poc.pnc.web;

import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.es.poc.pnc.model.Courier;
import com.es.poc.pnc.model.CouriersListForm;
import com.es.poc.pnc.model.Item;
import com.es.poc.pnc.model.User;
import com.es.poc.pnc.service.CourierService;
import com.es.poc.pnc.service.EmailService;
import com.es.poc.pnc.service.UserService;

/**
 * @author srinivas.minigula
 *
 */
@Controller
public class CourierController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private CourierService courierService;
	@Autowired
	UserService userService;
	@Autowired
	public EmailService emailService;
	@Autowired
	public SimpleMailMessage template;

	/**
	 * @param courierForm
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/courier/add", method = RequestMethod.POST)
	@Transactional
	public String add(@ModelAttribute("courierForm") Courier courierForm, Model model, Principal principal) {
		logger.info("courierForm={}", courierForm);
		logger.info("User Name={}", principal.getName());
		User user = userService.findByUsername(principal.getName());
		courierForm.setUser(user);
		if (user.getCourier() == null) {
			Set<Courier> courier = new HashSet<>();
			courier.add(courierForm);
			user.setCourier(courier);
		} else {
			user.getCourier().add(courierForm);
		}
		courierForm.setUser(user);
		List<Item> items = courierForm.getItems();
		items.forEach(p -> {
			p.setCourier(courierForm);
		});
		courierForm.setItems(items);
		Courier returnValue = courierService.save(courierForm);

		if (returnValue != null) {
			emailService.sendSimpleMessage(user.getEmail(), "Your Booking has been successful!" + returnValue.getId(),
					"Your Booking has been successful! Booking Id is:" + returnValue.getId() + "");
		}
		return "redirect:/welcome";
	}

	/**
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/courier/{id}", method = RequestMethod.GET)
	public ModelAndView getSingleCourier(@PathVariable("id") Long id) {
		Courier courier = courierService.findById(id);
		ModelAndView view = new ModelAndView("courier-details");
		view.addObject("courier", courier);
		return view;
	}

	/**
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/courier/search", method = RequestMethod.GET)
	public ModelAndView searchCourierWithID(@RequestParam("id") Long id) {
		Courier courier = courierService.findById(id);
		ModelAndView view = new ModelAndView("courier-details");
		view.addObject("courier", courier);
		return view;
	}

	/**
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = "/courier/all", method = RequestMethod.GET)
	public ModelAndView listAllByUserName(Model model, Principal principal) {
		ModelAndView view = new ModelAndView("couriersList");
		CouriersListForm form = new CouriersListForm();
		form.setCouriers(courierService.findByUser(userService.findByUsername(principal.getName())));
		view.addObject("courier", form);
		return view;
	}
}
