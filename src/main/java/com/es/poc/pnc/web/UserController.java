package com.es.poc.pnc.web;

import java.security.Principal;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.es.poc.pnc.model.Courier;
import com.es.poc.pnc.model.Item;
import com.es.poc.pnc.model.User;
import com.es.poc.pnc.service.SecurityService;
import com.es.poc.pnc.service.UserService;
import com.es.poc.pnc.validator.UserValidator;

/**
 * @author srinivas.minigula
 *
 */
@Controller
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private SecurityService securityService;
    @Autowired
    private UserValidator userValidator;

    /**
     * @param model
     * @return
     */
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new User());
        return "registration";
    }

    /**
     * @param userForm
     * @param bindingResult
     * @param model
     * @return
     */
    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        userService.save(userForm);

        securityService.autologin(userForm.getUsername(), userForm.getPasswordConfirm());
        model.addAttribute("userForm",  new User());
        return "redirect:/welcome";
    }

    /**
     * @param model
     * @param error
     * @param logout
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");
        
        model.addAttribute("userForm", new User());
        return "login";
    }

    /**
     * @param model
     * @param principal
     * @return
     */
    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model, Principal principal ) {
    		Courier courier = new Courier();
    		courier.setItems(new ArrayList<Item>());
    		User user = new User();
    		user.setUsername(principal.getName());
    		model.addAttribute("userForm", new User());
    	 model.addAttribute("courierForm",courier);
    	// 
        return "welcome";
    }
}
